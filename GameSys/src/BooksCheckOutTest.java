import static org.junit.Assert.*;
import java.math.BigDecimal;
import org.junit.Test;

public class BooksCheckOutTest {

	@Test
	public void checkOutBookAfter2000Test() {
		BooksCheckOut bookCheckOut = new BooksCheckOut();
		bookCheckOut.addSampleBooks();
		bookCheckOut.addToCart("The Terrible Privacy of Maxwell Sim");
		bookCheckOut.addToCart("Three Men in a Boat");

		assertEquals(new BigDecimal("24.69"),
				PriceCalculation.roundToPriceFormat(bookCheckOut.getCheckoutTotal()));
	}

	@Test
	public void checkOutNormalBookPricesSubTotalOver30Test() {
		BooksCheckOut bookCheckOut = new BooksCheckOut();
		bookCheckOut.addSampleBooks();
		bookCheckOut.addToCart("Still Life With Woodpecker");
		bookCheckOut.addToCart("Three Men in a Boat");
		bookCheckOut.addToCart("Great Expectations");

		assertEquals(new BigDecimal("35.27"),
				PriceCalculation.roundToPriceFormat(bookCheckOut.getCheckoutTotal()));
	}

	@Test
	public void checkOutBookAfter2000SubTotalOver30Test() {
		BooksCheckOut bookCheckOut = new BooksCheckOut();
		bookCheckOut.addSampleBooks();
		bookCheckOut.addToCart("The Terrible Privacy of Maxwell Sim");
		bookCheckOut.addToCart("Three Men in a Boat");
		bookCheckOut.addToCart("Great Expectations");

		assertEquals(new BigDecimal("36.01"),
				PriceCalculation.roundToPriceFormat(bookCheckOut.getCheckoutTotal()));
	}

}
