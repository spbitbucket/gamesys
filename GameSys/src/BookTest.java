import static org.junit.Assert.*;
import java.math.BigDecimal;
import org.junit.Test;

public class BookTest {

	@Test
	public void bookTitleValidationTest() {
		Book book = new Book("Moby Dick", 1851, new BigDecimal("15.20"));

		assertEquals("Moby Dick", book.getTitle());
	}

	@Test
	public void bookPriceValidationTest() {
		Book book = new Book("Moby Dick", 1851, new BigDecimal("15.20"));

		assertEquals(new BigDecimal("15.20"), book.getPrice());
	}

	@Test
	public void bookPublishYearValidationTest() {
		Book book = new Book("Moby Dick", 1851, new BigDecimal("15.20"));

		assertEquals(1851, book.getPublishYear());
	}

	@Test
	public void bookDiscountedPriceByYear1851Test() {
		Book book = new Book("Moby Dick", 1851, new BigDecimal("15.20"));

		assertEquals(new BigDecimal("15.20"), PriceCalculation.roundToPriceFormat(book
				.getDiscountedPriceYearAfter(2000, 10)));
	}

	@Test
	public void bookDiscountedPriceByYear2010Test() {
		Book book = new Book("The Terrible Privacy of Maxwell Sim", 2010,
				new BigDecimal("13.14"));

		assertEquals(new BigDecimal("11.82"), PriceCalculation.roundToPriceFormat(book
				.getDiscountedPriceYearAfter(2000, 10)));
	}
}
