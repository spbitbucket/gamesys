import java.math.BigDecimal;

public class Book {

	private String title;
	private int publishYear;
	private BigDecimal price;

	public Book(String title, int publishYear, BigDecimal price) {
		this.title = title;
		this.publishYear = publishYear;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String bookTitle) {
		this.title = bookTitle;
	}

	public int getPublishYear() {
		return publishYear;
	}

	public void setPublishYear(int bookPublishYear) {
		this.publishYear = bookPublishYear;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal bookPrice) {
		this.price = bookPrice;
	}

	public BigDecimal getDiscountedPriceYearAfter(int year, int percentageDicount) {
		if (getPublishYear() > year) {
			return PriceCalculation.getPercentageDiscount(price, percentageDicount);
		}

		return price;
	}

}
