import java.math.BigDecimal;

public class PriceCalculation {
	private static int DECIMALS = 2;
	private static int ROUNDING_MODE = BigDecimal.ROUND_DOWN;
	private static BigDecimal PERCENT_MULTIPLIER_VALUE = new BigDecimal("0.01");

	public static BigDecimal roundToPriceFormat(BigDecimal aNumber) {
		return aNumber.setScale(DECIMALS, ROUNDING_MODE);
	}

	public static BigDecimal getPercentageDiscount(BigDecimal amountOne,
			BigDecimal percentage) {
		BigDecimal result = amountOne.multiply(percentage);
		return amountOne.subtract(result);
	}

	public static BigDecimal getPercentageDiscount(BigDecimal amountOne,
			int percentage) {
		return getPercentageDiscount(amountOne,
				new BigDecimal(percentage).multiply(PERCENT_MULTIPLIER_VALUE));
	}

}
