import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BooksCheckOut {

	private List<Book> booksInventory;
	private List<Book> booksCheckOut;
	private BigDecimal subtotalExceedsCertainValue = new BigDecimal("30.00");
	private int subtotalExceedsPercentageDiscount = 5;
	private int discountApplyYearAfter = 2000;
	private int percentageDiscountApplyYearAfter = 10;

	public BooksCheckOut() {
		this.booksInventory = new ArrayList<Book>();
		this.booksCheckOut = new ArrayList<Book>();
	}

	public void addSampleBooks() {
		booksInventory
				.add(new Book("Moby Dick", 1851, new BigDecimal("15.20")));
		booksInventory.add(new Book("The Terrible Privacy of Maxwell Sim",
				2010, new BigDecimal("13.14")));
		booksInventory.add(new Book("Still Life With Woodpecker", 1980,
				new BigDecimal("11.05")));
		booksInventory.add(new Book("Sleeping Murder", 1976, new BigDecimal(
				"10.24")));
		booksInventory.add(new Book("Three Men in a Boat", 1889,
				new BigDecimal("12.87")));
		booksInventory.add(new Book("The Time Machine", 1895, new BigDecimal(
				"10.43")));
		booksInventory.add(new Book("The Caves of Steel", 1954, new BigDecimal(
				"8.12")));
		booksInventory.add(new Book("Idle Thoughts of an Idle Fellow", 1886,
				new BigDecimal("7.32")));
		booksInventory.add(new Book("A Christmas Carol", 1843, new BigDecimal(
				"4.23")));
		booksInventory.add(new Book("A Tale of Two Cities", 1859,
				new BigDecimal("6.32")));
		booksInventory.add(new Book("Great Expectations", 1861, new BigDecimal(
				"13.21")));
	}

	public void addToCart(String bookName) {
		for (Book book : booksInventory) {
			if (book.getTitle().equals(bookName)) {
				booksCheckOut.add(book);
			}
		}
	}

	public BigDecimal getCheckoutTotal() {
		BigDecimal totalPrice = new BigDecimal("0.0");
		
		for (Book book : booksCheckOut) {
			System.out.println(book.getTitle() + " (" + book.getPublishYear()
					+ ") " + book.getPrice());
			totalPrice = totalPrice.add(book.getDiscountedPriceYearAfter(
					discountApplyYearAfter, percentageDiscountApplyYearAfter));
		}

		if (totalPrice.compareTo(subtotalExceedsCertainValue) == 1) {
			return PriceCalculation.roundToPriceFormat(PriceCalculation
					.getPercentageDiscount(totalPrice,
							subtotalExceedsPercentageDiscount));
		} else {
			return PriceCalculation.roundToPriceFormat(totalPrice);
		}
	}
	
	public void getCheckOut() {
		System.out.println("Total Price: " + getCheckoutTotal() + "\n");
	}

	public static void main(String[] args) {
		BooksCheckOut bookCheckOut = new BooksCheckOut();
		bookCheckOut.addSampleBooks();
		bookCheckOut.addToCart("The Terrible Privacy of Maxwell Sim");
		bookCheckOut.addToCart("Three Men in a Boat");
		bookCheckOut.getCheckOut();

		BooksCheckOut bookCheckOut2 = new BooksCheckOut();
		bookCheckOut2.addSampleBooks();
		bookCheckOut2.addToCart("Still Life With Woodpecker");
		bookCheckOut2.addToCart("Three Men in a Boat");
		bookCheckOut2.addToCart("Great Expectations");
		bookCheckOut2.getCheckOut();

		BooksCheckOut bookCheckOut3 = new BooksCheckOut();
		bookCheckOut3.addSampleBooks();
		bookCheckOut3.addToCart("The Terrible Privacy of Maxwell Sim");
		bookCheckOut3.addToCart("Three Men in a Boat");
		bookCheckOut3.addToCart("Great Expectations");
		bookCheckOut3.getCheckOut();
	}

}
